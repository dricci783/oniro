# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

require conf/distro/include/security_flags.inc

INHERIT += "oniro-sanity"
