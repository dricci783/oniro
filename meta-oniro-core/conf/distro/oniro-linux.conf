# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

DISTRO = "oniro-linux"
DISTRO_NAME = "Oniro Project Base Linux Distro"
DISTRO_VERSION = "1.99.99"
DISTRO_CODENAME = "dev"
SDK_VENDOR = "-onirosdk"
SDK_VERSION = "${@d.getVar('DISTRO_VERSION').replace('snapshot-${DATE}', 'snapshot')}"

MAINTAINER = "Oniro Project <andrei.gherzan@huawei.com>"

TARGET_VENDOR = "-oniro"

LOCALCONF_VERSION = "2"

DISTRO_VERSION[vardepsexclude] = "DATE"
SDK_VERSION[vardepsexclude] = "DATE"

# Override these in oniro based distros
ONIRO_DEFAULT_DISTRO_FEATURES = "largefile opengl ptest multiarch pam rauc security wayland vulkan"
ONIRO_DEFAULT_EXTRA_RDEPENDS = "packagegroup-core-boot"
ONIRO_DEFAULT_EXTRA_RRECOMMENDS = "kernel-module-af-packet"

# Remove some of the default Poky DISTRO_FEATURES_DEFAULT we inherited, but do not use.
# Removing this here allows us to readd them, if the end user requires them.
DISTRO_FEATURES_DEFAULT:remove = " nfs nfc 3g pcmcia x11"

DISTRO_FEATURES ?= "${DISTRO_FEATURES_DEFAULT} ${ONIRO_DEFAULT_DISTRO_FEATURES}"

SDK_NAME = "${DISTRO}-${TCLIBC}-${SDKMACHINE}-${IMAGE_BASENAME}-${TUNE_PKGARCH}-${MACHINE}"
SDKPATHINSTALL = "/opt/${DISTRO}/${SDK_VERSION}"

DISTRO_EXTRA_RDEPENDS += " ${ONIRO_DEFAULT_EXTRA_RDEPENDS}"
DISTRO_EXTRA_RRECOMMENDS += " ${ONIRO_DEFAULT_EXTRA_RRECOMMENDS}"

TCLIBCAPPEND = ""

PREMIRRORS ??= "\
bzr://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
cvs://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
git://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
gitsm://.*/.* http://downloads.yoctoproject.org/mirror/sources/ \n \
hg://.*/.*    http://downloads.yoctoproject.org/mirror/sources/ \n \
osc://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n \
p4://.*/.*    http://downloads.yoctoproject.org/mirror/sources/ \n \
svn://.*/.*   http://downloads.yoctoproject.org/mirror/sources/ \n"

SANITY_TESTED_DISTROS ?= " \
            ubuntu-18.04 \n \
            ubuntu-20.04 \n \
            "

require conf/distro/include/no-static-libs.inc
require conf/distro/include/yocto-uninative.inc
INHERIT += "uninative"

BB_SIGNATURE_HANDLER ?= "OEEquivHash"
BB_HASHSERVE ??= "auto"

require conf/distro/include/oniro.inc
require conf/distro/include/oniro-wic.inc
require conf/distro/include/oniro-packageconfig.inc

TCLIBC = "musl"

INIT_MANAGER = "systemd"

PREFERRED_VERSION_linux-yocto = "5.10%"
PREFERRED_PROVIDER_virtual/kernel:qemuarm = "linux-oniro"
PREFERRED_PROVIDER_virtual/kernel:qemuarm64 = "linux-oniro"
PREFERRED_PROVIDER_virtual/kernel:qemux86 = "linux-oniro"
PREFERRED_PROVIDER_virtual/kernel:qemux86-64 = "linux-oniro"

# qemu-generic-arm64 has issues booting past 5.10. See:
# https://git.yoctoproject.org/meta-arm/tree/meta-arm/conf/machine/generic-arm64.conf?id=c40fb5348b1d0f8c4a1ed779c8df6ba3cf411930
# When kernel version is bumped we will have to revisit here.
PREFERRED_PROVIDER_virtual/kernel_qemu-generic-arm64 = "linux-oniro"

# Prefer GRUB as the EFI provider.
#
# The SysOTA+RAUC stack supports GRUB as the point of integration. This is
# caused by both RAUC downstream support for GRUB and GRUB's ability to load the
# kernel from a squashfs image.
#
# FIXME: allow GRUB to update the CPU microcode
# https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/508
EFI_PROVIDER = "grub-efi"
# Add EFI to x86{,-64} QEMU machine features.
MACHINE_FEATURES:qemux86-64 += "efi"
MACHINE_FEATURES:qemux86 += "efi"

# Add bootindex to qemu boot device to prevent boot order from being changed in ovmf file
# and not break boot order with EFI. See qemuboot.bbclass for more information
QB_ROOTFS_EXTRA_OPT:qemux86-64 += ",bootindex=0"
QB_ROOTFS_EXTRA_OPT:qemux86 += ",bootindex=0"

# Install the grubenv file deployed by oniro-grub-bootconf package into the EFI partition.
IMAGE_EFI_BOOT_FILES += "grubenv;EFI/BOOT/grubenv"

# Prefer Oniro-specific GRUB configuration file.
# (note r-provider is used below)
PREFERRED_RPROVIDER_virtual-grub-bootconf = "oniro-grub-bootconf"

# qemu-generic-arm64 specific requirements
PREFERRED_VERSION_optee-os:qemu-generic-arm64 = "3.14.0"

# Default to enabling serial debug console on RaspberryPi
ENABLE_UART ?= "1"

GPU_MEM:raspberrypi4-64 = "128"

SPLASH = "psplash-oniro"
